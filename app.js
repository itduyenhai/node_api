const express = require('express');
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const passport = require('passport');
const cfg = require('./config/main');

// Var route
const v1 = require('./api/v1/index');

const app = express();
const router = express.Router();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// Passport init
app.use(passport.initialize());

// Mongoose connect + Promise
mongoose.Promise = global.Promise;
mongoose.connect(cfg.database, {useMongoClient: true})
	.then(() => {
		console.log('DB connect ...');
	})
	.catch(err => console.error(err));

require('./config/passport')(passport);
// Auth: Login - Register
// Require Auth Controllers
const trl = require('./api/v1/controllers');
// Auth
router.post('/login', ctrl.auth.login)
router.post('/register', ctrl.auth.register)

// Authenticate passport-jwt
// app.use(passport.authenticate());
// Use route
app.use('/api/v1', v1);

module.exports = app;
