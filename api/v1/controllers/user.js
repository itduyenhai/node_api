const User = require('../models/user');

module.exports = {
  // Get all
  get: (req, res) => {
    User.find()
      .then((user) => {
        if(!user){
          res.status(401).json({message:'Không có người dùng'})
        }
        res.status(200).json(user);
      })
      .catch((err) =>{
        res.status(404).json({message:'Lỗi ứng dụng'})
      })
  },

  // Profile user:id
  profile: {

  },

  // Create user
  create: {

  },

  // Update user
  update: {

  },

  // Delete user
  delete: {

  }
};
