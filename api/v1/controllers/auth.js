const User = require('../models/user');
const jwt = require('jsonwebtoken');
const cfg = require('../../../config/main');

module.exports = {

  // Login
  login: (req, res) => {
    User.findOne({
      email: req.body.email
    }, (err, user) => {
      if (err) throw err;

      if (!user) {
        res.status(404).json({ message: 'Email không tồn tại' });
      } else {
        // Check if password matches
        user.comparePassword(req.body.password, (err, isMatch) => {
          if (isMatch && !err) {
            // Create token if the password matched and no error was thrown
            var token = jwt.sign(user, cfg.secret, {
              expiresIn: 10080 // in seconds
            });
            res.status(200).json({ token: 'JWT ' + token });
          } else {
            res.status(500).json({ message: 'Đăng nhập thất bại' });
          }
        });
      }
    });
  },

  // Register
  register: (req, res) => {
    // Check email - password
    if(!req.body.email || !req.body.password) {
      res.status(404).json({ message: 'Hãy nhập email và password' });
    } else {
      // Create User
      var newUser = new User({
        email: req.body.email,
        password: req.body.password
      });

      // Attempt to save the user
      newUser.save()
      .then(user => {
        res.status(201).json({ message: 'Đăng ký thành công' });
      })
      .catch(err => {
        return res.status(500).json({ message: 'Email đã tồn tại'});
      });
    }
  }
};
