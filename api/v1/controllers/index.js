module.exports = {
  auth = require('./auth'),
  user = require('./user'),
  role = require('./role'),
  permission = require('./permission')
};
