const express = require('express');
const router = express.Router();

// Require Controllers
const ctrl = require('./controllers');
// const roleCtrl = require('./controllers/role');
// const perrmissionCtrl = require('./controllers/permission');

// Home
router.get('/', (req, res) => {
  res.status(200).json({message: 'Home index'})
})

// User
router.get('/users', ctrl.user.get);

module.exports = router;
