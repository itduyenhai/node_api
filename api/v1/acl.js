module.exports = {
  permission: {
    'user': {
      'all': 'Tất cả người dùng',
      'profile': 'Hiển thị người dùng',
      'active': 'Kích hoạt người dùng',
      'create': 'Thêm người dùng',
      'update': 'Cập nhật người dùng',
      'delete': 'Xóa người dùng',
    },
    'role': {
      'all': 'Tất cả vai trò',
      'show': 'Hiển thị vai trò',
      'create': 'Thêm vai trò',
      'update': 'Cập nhật vai trò',
      'delete': 'Xóa vai trò'
    },
    'permission': {
      'all': 'Tất cả quyền hạn',
      'show': 'Hiển thị quyền hạn',
      'create': 'Thêm quyền hạn',
      'update': 'Cập nhật quyền hạn',
      'delete': 'Xóa quyền hạn'
    }
  },

  role: {
    'administrator':'administrator'
  }
};
